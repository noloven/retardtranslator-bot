const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const fs = require('fs');

var count = 0;

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);

});

client.on('message', msg => {

    if(msg.author.bot) {
        return;
    }

    /*
    if(msg.content.indexOf(config.prefix) !== 0) {
        return;
    }
    */
    const args = msg.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    //console.log("Author: ", msg.author);
    console.log("Last Message ID: ", msg.author.lastMessage.id);
    console.log("Last Message Content: ", msg.author.lastMessage.content);

    //console.log("Author: ", msg.author);
    //if(msg.author.username === "WarmApplePie" && msg.author.discriminator === "3266") {
    if(msg.author.username === "WarmApplePie" && msg.author.discriminator === "3266" || msg.author.username === "WarmApplePieTG" && msg.author.discriminator === "1478") {
        console.log("Match for Author! " + msg.author.username + " " + msg.author.discriminator);
        const content = msg.content.slice().trim().split(/ +/g);
        //console.log("Content: ", content);
        //console.log("Content Length: ", content.length);
        var alertWordList = fs.readFileSync("./alert_list.txt").toString('utf-8');
        var wordList = alertWordList.split("\r\n");
            
        
        for(let contentCount = 0; contentCount < content.length; contentCount++) {
            for(let wordListCount = 0; wordListCount < wordList.length; wordListCount++) {
                var chars = wordList[wordListCount].split('');
                //console.log("chars: ", chars);
                var regex = "";
                for(let charCount = 0; charCount < chars.length; charCount++) {
                    var regex = regex + chars[charCount] + "{1,20}";
                }
                    
                console.log("Regular Expression: ", regex);
                regex1 = RegExp(regex, "gi");
                console.log("Content: ", content[contentCount]);
                if(regex1.test(content[contentCount]) === true) {
                    console.log("Alert word found: ", content[contentCount]);
                    msg.delete(3000)
                        .then(msg => console.log(`Deleted message from ${msg.author.username} + ":" + ${msg.author.discriminator}`))
                        .catch(console.error);
                    
                    count = count + 1;
                    console.log("Count: ", count);
                    return;
                }    
            }
        }   
    }
})

client.login(config.token);